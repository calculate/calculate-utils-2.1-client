#!/usr/bin/env python

# setup.py --- Setup script for calculate-client

#Copyright 2008 Calculate Pack, http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import stat
from distutils.core import setup, Extension
from distutils.command.install_data import install_data

module1 = Extension('calculate-client.pym._cl_keys',
                    library_dirs = ['/usr/lib'],
                    libraries = ['keyutils'],
                    sources = ['./lib/cl_keys.i','./lib/cl_keys.c'])

data_files = []

var_data_files = []

data_dirs_local = ['client-profiles', 'profiles']
share_calculate_dir = "/usr/share/calculate-2.0/"
data_dirs_share = ['i18n']


def __scanDir(scanDir, prefix, dirData, flagDir=False):
    """Scan directory"""
    files = []
    dirs = []
    if flagDir or stat.S_ISDIR(os.stat(scanDir)[stat.ST_MODE]):
        for fileOrDir in os.listdir(scanDir):
            absPath = os.path.join(scanDir,fileOrDir)
            statInfo = os.stat(absPath)[stat.ST_MODE]
            if stat.S_ISREG(statInfo):
                files.append(absPath)
            elif stat.S_ISDIR(statInfo):
                dirs.append(absPath)
        if prefix:
            scanDir = os.path.join(prefix,scanDir)
        dirData.append((scanDir, files))
        for sDir in dirs:
            __scanDir(sDir, prefix, dirData, True)
    return dirData

def create_data_files(data_dirs, prefix=""):
    """Create data_files"""
    data_files = []
    for data_dir in data_dirs:
        data = []
        data_files += __scanDir(data_dir, prefix, data)
    return data_files

data_files += create_data_files (data_dirs_local)
data_files += create_data_files (data_dirs_share, share_calculate_dir)
data_files += [('/etc/init.d', ['data/client'])]
data_files += [('/usr/share/calculate/xdm', ['data/login',
                                             'data/logout',
                                             'data/gtkbg',
                                             'data/cmd_login'])]
class cl_install_data(install_data):
    def run (self):
        install_data.run(self)
        data_file = [("/etc/init.d/client",0755),
                     ("/usr/share/calculate/xdm/login",0755),
                     ("/usr/share/calculate/xdm/logout",0755),
                     ("/usr/share/calculate/xdm/gtkbg",0755),
                     ("/usr/share/calculate/xdm/cmd_login",0755)]
        fileNames = map(lambda x: os.path.split(x[0])[1], data_file)
        listNames = map(lambda x: filter(lambda y: y, x[0].split("/")),data_file)
        data_find = {}
        for i in range(len(fileNames)):
            listNames[i].reverse()
            data_find[fileNames[i]] =[listNames[i],data_file[i][1]]

        for path in self.get_outputs():
            nameFile = os.path.split(path)[1]
            if nameFile in data_find.keys():
                data = data_find[nameFile][0]
                mode = data_find[nameFile][1]
                flagFound = True
                iMax = len(data)
                pathFile = path
                for i in range(iMax):
                    if data[i] != os.path.split(pathFile)[1]:
                        flagFound = False
                        break
                    pathFile = os.path.split(pathFile)[0]
                if flagFound:
                    os.chmod(path, mode)

setup(
    name = 'calculate-client',
    version = "2.1.18",
    description = "Client for the calculate-server",
    author = "Mir Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    package_dir = {'calculate-client': "."},
    packages = ['calculate-client.pym'],
    data_files = data_files,
    scripts=["./scripts/cl-createhome",
             "./scripts/cl-client",
             "./scripts/cl-sync",
             "./scripts/cl-passwd"],
    ext_modules = [module1],
    cmdclass={'install_data': cl_install_data},
)
