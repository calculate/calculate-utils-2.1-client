//  Copyright 2007-2010 Mir Calculate Ltd. http://www.calculate-linux.org
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>

// для пароля
#include <sys/types.h>

// для strcpy strlen
#include <string.h>

// для работы с ключами
#include <keyutils.h>

char* getKey(char *login)
{
  char buffer[255];
  memset(buffer,0,sizeof(buffer));
  int ret;
      // ищем номер пользовательского ключа
      ret = request_key("user", login, NULL, 0);
      if (ret < 0)
        {
//           printf ("id_key not found\n");
          return "";
        };

      // Возвращаем значение ключа
       ret = keyctl_read(ret, buffer, sizeof(buffer));
      if (ret < 0)
        {
//           printf("error keyctl_read\n");
          return "";
        };
  return buffer;
};

int clearKey(char *login)
{
    char *buffer;
    buffer = "XXXXXXXX";
    key_serial_t dest;
    dest = KEY_SPEC_USER_SESSION_KEYRING;
    if (add_key("user", login, buffer, strlen(buffer), dest)!=-1)
      return 0;
    else
      return 1;
};
