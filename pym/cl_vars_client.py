#-*- coding: utf-8 -*-

# Copyright 2008-2010 Mir Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи значений
#   mode -     режим переменной r-не переназначается из командной строки,
#              w-переназначается из командной строки
#   type     - тип переменной состоит из двух элементов(что это и для чего
#              это)
#   value    - дефоултное значение переменной
#   select   - список допустимых значений переменной
#   official - флаг того, что данная переменная служебная и не отображается
#              при печати списка значений переменных
#   printval - печатное значение переменной(значение выводимое при печати
#              списка значений переменных)

class Data:
    #базовый суффикс LDAP
    ld_base_dn = {}

    #DN всех сервисов
    ld_services_dn =  {}

    #DN админстратора сервиса Unix (он, же DN сервиса)
    ld_unix_dn = {}

    #DN админстратора сервиса Samba (он, же DN сервиса)
    ld_samba_dn = {}

    #bind суффикс LDAP
    ld_bind_dn = {}

    #пароль для пользователя для чтения
    ld_bind_pw = {}

    # имя компьютера с настроенным сервисом Mail
    sr_mail_host = {'mode':"w"}

    # шифрование при получении - ''/ssl/tls
    sr_mail_crypt = {'mode':"w"}
    # порт получения
    sr_mail_port = {'mode':"w"}
    # тип получения - pop/imap
    sr_mail_type = {'mode':"w"}
    # шифрование отправки - ''/ssl/tls
    sr_mail_send_crypt = {'mode':"w"}
    # порт отправки
    sr_mail_send_port = {'mode':"w"}
    # хост отправки
    sr_mail_send_host = {'mode':"w"}

    # имя компьютера с настроенным сервисом Samba
    sr_samba_host = {'mode':"w"}

    # имя компьютера с настроенным сервисом Jabber
    sr_jabber_host = {'mode':"w"}

    # шифрование при получении - ''/ssl
    sr_jabber_crypt = {'mode':"w"}

    # порт jabber сервиса
    sr_jabber_port = {'mode':"w"}

    # имя компьютера с настроенным сервисом FTP
    cl_remote_ftp = {'mode':"w"}

    #путь к директории относительно которой происходит наложение профилей на
    #файлы системы
    cl_root_path = {}

    ##список накладываемых профилей при установке, наложении профилей
    cl_profile_path = {}

    #Полное имя LDAP пользователя
    ur_fullname = {'mode':"w"}

    # Почтовый адрес пользователя
    ur_mail = {'mode':"w"}

    # Host Jabber пользователя
    ur_jid_host = {'mode':"w"}

    # Jabber ID пользователя
    ur_jid = {'mode':"w"}

    #Разрешение X по вертикали
    hr_x11_height = {'mode':"w"}

    #Разрешение X по горизонтали
    hr_x11_width = {'mode':"w"}

    #Пароль пользователя client
    cl_remote_pw = {'mode':'w'}

    # ip или имя домена (под управлением calculate-server)
    cl_remote_host = {'mode':'w'}

    # ближайший стандартный размер изображения к текущему разрешению
    hr_x11_standart = {}

    # Если компьютер ноутбук, то его производитель
    hr_laptop = {}

    # Название производителя видеокарты
    hr_video = {}

    # Video driver used by xorg
    hr_x11_video_drv = {}

    # Включен ли композитный режим видеокарты on/off
    hr_x11_composite = {}

    #Название группы пользователя
    ur_group = {'mode':"w"}

    #Организация пользователя
    ur_organization = {'mode':'w'}

    #Сигнатура пользователя
    ur_signature = {'mode':'w'}

    # DN ветки репликации
    ld_repl_dn = {}

    # имя программы
    cl_name = {'value':'calculate-client'}

    # версия программы
    cl_ver = {'value':'2.1.18'}

    # Режим работы клиента клиент
    # (local или имя хоста клиента)
    os_remote_auth = {'mode':'w'}

    # Версия программы которой был наложен профиль
    os_remote_client = {'mode':'w'}

    # имя компьютера с настроенным сервисом Proxy
    sr_proxy_host = {'mode':"w"}

    # порт подключения к сервису Proxy
    sr_proxy_port = {'mode':"w"}
