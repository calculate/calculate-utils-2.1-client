#-*- coding: utf-8 -*-

# Copyright 2008-2010 Mir Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import re
import cl_base


class fillVars(object, cl_base.glob_attr):

    def get_cl_profile_path(self):
        """список накладываемых профилей при установке, наложении профилей"""
        profpath = []
        profPaths=['/usr/lib/calculate/calculate-client/client-profiles',
                     '/var/calculate/remote/client-profiles',
                     '/var/calculate/client-profiles']
        for profPath in profPaths:
            if os.path.isdir(profPath):
                paths = os.listdir(profPath)
                for path in paths:
                    ph = os.path.join(profPath,path)
                    if os.path.isdir(ph) and os.listdir(ph):
                        profpath.append(ph)
        return profpath

    def getX11Resolution(self):
        """возвращает текущее разрешение экрана (ширина, высота), X запущен"""
        lines=self._runos("xdpyinfo")
        if not lines:
            return ""
        reRes = re.compile("dimensions:\s+(\d+)x(\d+)\s+pixels")
        searchRes=False
        for line in lines:
            searchRes = reRes.search(line)
            if searchRes:
                break
        if searchRes:
            return (searchRes.group(1), searchRes.group(2))
        else:
            return ""

    def get_hr_x11_height(self):
        """Получить высоту экрана в пикселах"""
        resolution = self.getX11Resolution()
        if resolution:
            self.Set('hr_x11_width',resolution[0])
            return resolution[1]
        return "768"

    def get_hr_x11_width(self):
        """Получить ширину экрана в пикселах"""
        resolution = self.getX11Resolution()
        if resolution:
            self.Set('hr_x11_height',resolution[1])
            return resolution[0]
        return "1024"

    def get_hr_x11_standart(self):
        """Получить ближайший стандартный размер изображения к текущему разрешению"""
        #Стандартные разрешения
        widthVal = self.Get('hr_x11_width')
        heightVal = self.Get('hr_x11_height')
        if not widthVal or not heightVal:
            return ""
        width = int(widthVal)
        height = int(heightVal)
        res = [(1024,768),
               (1280,1024),
               (1280,800),
               (1440,900),
               (1600,1200),
               (1680,1050),
               (1920,1200)]
        resolution = []
        formats = []
        for w, h in res:
            formats.append(float(w)/float(h))
        listFr = list(set(formats))
        listFormats = {}
        for fr in listFr:
            listFormats[fr] = []
        for w, h in res:
            for fr in listFormats.keys():
                if fr == float(w)/float(h):
                    listFormats[fr].append((w,h))
                    break
        format = float(width)/float(height)
        deltaFr = {}
        for fr in listFormats.keys():
            deltaFr[abs(format - fr)] = fr
        resolution = listFormats[deltaFr[min(deltaFr.keys())]]
        flagFound = False
        stResol = []
        stHeights = []
        stWidths = []
        stWidth = False
        stHeight = False
        for w, h in resolution:
            if w >= width and h >= height:
                stResol.append((w,h))
                stHeights.append(h)
        if stHeights:
            stHeight = min(stHeights)
            for w, h in stResol:
                if stHeight ==  h:
                    stWidths.append(w)
            if stWidths:
                stWidth = min(stWidths)
        if (not stWidth) or (not stHeight):
            return "%sx%s"%(resolution[-1][0],resolution[-1][1])
        else:
            return "%sx%s"%(stWidth,stHeight)

    def get_hr_x11_composite(self):
        """Включен ли композитный режим видеокарты on/off"""
        xorgConfig = "/etc/X11/xorg.conf"
        try:
            confLines = open(xorgConfig,"r").readlines()
        except:
            return "off"
        flagStartExtensions = False
        lineCompositeTmp = ""
        lineComposite = ""
        for line in confLines:
            if flagStartExtensions:
                if 'EndSection' in line:
                    lineComposite = lineCompositeTmp
                    break
                elif 'Section' in line:
                    break
                if 'Option' in line and '"Composite"' in line:
                    lineCompositeTmp = line
            else:
                if '"Extensions"' in line and 'Section' in line:
                    flagStartExtensions = True
        if lineComposite:
            listOpt = filter(lambda x: x.strip(), lineComposite.split('"'))
            if len(listOpt) == 3:
                ret = listOpt[2].lower()
                if ret in ("on","off"):
                    return ret
        return "off"
    

    def get_hr_laptop(self):
        """Если компьютер ноутбук, то его производитель"""
        formfactor = self._runos("hal-get-property --udi \
/org/freedesktop/Hal/devices/computer --key system.formfactor")
        if not formfactor:
            return ""
        if formfactor == 'laptop':
            vendor = self._runos("hal-get-property --udi \
/org/freedesktop/Hal/devices/computer --key system.hardware.vendor")
            if vendor:
                vendor = vendor.split(" ")[0]
            else:
                vendor = "unknown"
            return vendor.lower()
        return ""

    def get_hr_x11_video_drv(self):
        """Get video driver used by xorg"""
        xorg_modules_dir = '/usr/lib/xorg/modules/drivers'
        xorg_conf = '/etc/X11/xorg.conf'
        # Try analize Xorg.{DISPLAY}.log
        display = os.environ.get('DISPLAY')
        if display and os.path.exists(xorg_modules_dir):
            list_avialable_drivers = os.listdir(xorg_modules_dir)
            if list_avialable_drivers:
                reDriver = re.compile('|'.join(list_avialable_drivers))
                display_number = re.search(r':(\d+)\..*', display)
                if display_number:
                    xorg_log_file = '/var/log/Xorg.%s.log' % \
                                    display_number.group(1)
                    if os.path.exists(xorg_log_file):
                        matchStrs = [i for i in open(xorg_log_file) 
                                        if "drv" in i and reDriver.search(i)]
                        if matchStrs:
                            resDriver = re.search(r'([^/]+)_drv.so',
                                matchStrs[-1])
                            if resDriver:
                                return resDriver.group(1)

        # analize /etc/X11/xorg.conf
        if os.path.exists(xorg_conf):
            matchSect = re.search(r'Section "Device".*?EndSection',
                                  open('/etc/X11/xorg.conf').read(),re.S)
            if matchSect:
                resDriver = re.search(r'Driver\s*"([^"]+)"',
                                      matchSect.group(0),re.S)
                if resDriver:
                    return resDriver.group(1)
        return "vesa"

    def get_hr_video(self):
        """Производитель видеокарты"""
        lines=self._runos("lspci")
        if not lines:
            return ""
        reVGA = re.compile("vga",re.I)
        foundVGA = False
        for line in lines:
            if reVGA.search(line):
                foundVGA = True
                break
        if not foundVGA:
            return "vesa"
        if "nVidia" in line or "GeForce" in line:
            return "nvidia"
        elif "ATI" in line:
            return "ati"
        elif "Intel" in line:
            return "intel"
        elif "VIA" in line:
            return "via"
        elif "VMware" in line:
            return "vmware"
        else:
            return "vesa"

    def get_ur_jid_host(self):
        """Host Jabber пользователя"""
        userJid = self.Get("ur_jid")
        if userJid:
            return userJid.partition('@')[2]
        return ""
