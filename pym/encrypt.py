#-*- coding: utf-8 -*-

# Copyright 2008-2010 Mir Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
import os
import hashlib
import crypt
import string
import time
from random import choice
from base64 import encodestring as b64encode
import smbpasswd
import cl_base
# Перевод модуля
tr = cl_base.lang()
tr.setLanguage(sys.modules[__name__])

class encrypt:
    """Класс хранения общих методов используемых для настройки сервисов

    Методы шифрования, создания сертификатов и.т. д
    """

    def __GenCryptSalt__(self):
        """Генерация соли для хеширования пароля (CRYPT)"""
        chars = string.letters + string.digits + "./"
        salt = ""
        for i in range(2):
            salt = salt + choice(chars)
        return salt

    def getHashPasswd(self, password, SecHashAlg):
        """Генерация хеша пароля,

        Поддерживаемые алгоритмы шифрования пароля:
        plain, md5, smd5, crypt, sha, ssha, lm, nt
        """
        if not password:
            print _("ERROR") + " getHashPasswd: " +\
                            _("password empty")
            return False

        hashPwd = ""
        if SecHashAlg == "plain":
            hashPwd = password

        elif SecHashAlg == "md5":
            h = hashlib.md5(password)
            hashPwd = "{MD5}" + b64encode(h.digest())

        elif SecHashAlg == "smd5":
            salt = os.urandom(4)
            h = hashlib.md5(password)
            h.update(salt)
            hashPwd = "{SMD5}" + b64encode(h.digest() + salt)

        elif SecHashAlg == "crypt":
            salt = self.__GenCryptSalt__()
            hashPwd = "{CRYPT}" + crypt.crypt(password, salt)

        elif SecHashAlg == "sha":
            h = hashlib.sha1(password)
            hashPwd = "{SHA}" + b64encode(h.digest())

        elif SecHashAlg == "ssha":
            salt = os.urandom(4)
            h = hashlib.sha1(password)
            h.update(salt)
            hashPwd = "{SSHA}" + b64encode(h.digest() + salt)

        elif SecHashAlg == "lm":
            hashPwd = smbpasswd.lmhash(password)

        elif SecHashAlg == "nt":
            hashPwd = smbpasswd.nthash(password)

        else:
            print _("ERROR") + " getHashPasswd: " +\
                            _("Can not support '%s' crypto algorithm")%SecHashAlg
            return False
        return hashPwd
